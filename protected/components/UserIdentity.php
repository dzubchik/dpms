<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$users = array(
			'admin' => 'ILoveQuantumMechanics',
		);
		if ($this->username == 'admin') {
			if (!isset($users[$this->username]))
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			else if ($users[$this->username] !== $this->password) {
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			}
			else {
				$this->errorCode = self::ERROR_NONE;
				$this->setState('id', 0); // user is admin
			}
		} else if (!$user = Yii::app()->db->createCommand()->select('id,email,name,surname')->from('users')->where(array("and", "`email`='" . $this->username . "'", "`password`='" . hash('sha512', $this->password) . "'"))->queryRow()) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->errorCode = self::ERROR_NONE;
			$this->setState('email', $user['email']);
			$this->setState('id', $user['id']);
		}
		return !$this->errorCode;
	}
}
