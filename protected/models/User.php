<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $surname
 * @property string $middle_name
 * @property string $password
 * @property string $info
 * @property string $reason
 * @property integer $year
 * @property string $university
 */
class User extends CActiveRecord
{
	public $full_name;
	const LEVEL_BACHELOR = 0,
		LEVEL_MASTER = 1,
		LEVEL_SPECIAL = 2;


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, name, surname, middle_name, info, year, enter, level', 'required', 'message' => '{attribute} не може бути порожнім'),
			array('password', 'required', 'on' => 'create'),
			array('year', 'numerical', 'min' => '1996', 'max' => date('Y')),
			array('enter', 'numerical', 'min' => '1991', 'max' => date('Y')),
			array('email', 'length', 'max' => 50),
			array('level', 'numerical', 'max' => 0, 'max' => 2, 'integerOnly' => true),
			array('email', 'unique'),
			array('name, surname, middle_name,photo', 'length', 'max' => 30),
			array('password', 'length', 'max' => 128),
			array('university,wish, reason, additional', 'length', 'max' => 2000),
			array('publications', 'length', 'max' => 4000),
			array('url', 'url'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, name, surname, middle_name, password, info, reason, year, university', 'safe', 'on' => 'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'           => 'ID',
			'email'        => 'Email',
			'name'         => 'Ім&prime;я',
			'surname'      => 'Прізвище',
			'middle_name'  => 'По-батькові',
			'password'     => 'Пароль',
			'info'         => 'Інформація про вас (CV) [де вчились/ працювали/ набирались досвіду і розуму]',
			'reason'       => 'Чому варто навчатись на кафедрі фіз-мат наук  НаУКМА',
			'year'         => 'Рік випуску',
			'university'   => 'Де навчаєтесь/ працюєте на даний момент',
			'wish'         => 'Що б Ви хотіли порадити студентам нашої кафедри?',
			'additional'   => 'Додаткові відомості про вас (інтереси, хоббі) [додавайте посилання]',
			'url'          => 'Сайт, блог або сторінка у соціальній мережі',
			'enter'        => 'Рік вступу',
			'publications' => 'Публікації, Ваші проекти і т.д.',
			'level'        => 'Освітній рівень'
		);
	}

	public function afterFind()
	{
		$this->full_name = $this->surname . ' ' . $this->name;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('email', $this->email, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('surname', $this->surname, true);
		$criteria->compare('middle_name', $this->middle_name, true);
		$criteria->compare('info', $this->info, true);
		$criteria->compare('reason', $this->reason, true);
		$criteria->compare('year', $this->year);
		$criteria->compare('university', $this->university, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function getLevels()
	{
		return array(User::LEVEL_BACHELOR => "Бакалавр", User::LEVEL_MASTER => "Магістр", User::LEVEL_SPECIAL => "Спеціаліст");
	}

	public static function getPhoto($id, $thumb = false)
	{
		$model = User::model()->findByPk($id);
		return (strlen($model->photo)>1)
			? Yii::app()->params['uploadFolder'] . ($thumb ? 't/t' : '') . $model->photo
			:"http://placehold.it/".($thumb?'64x64':'240x320')."/000000";
	}
}
