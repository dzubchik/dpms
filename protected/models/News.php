<?php

/**
 * This is the model class for table "{{news}}".
 *
 * The followings are the available columns in table '{{news}}':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $deadline
 * @property integer $author
 * @property integer $created
 * @property integer $status
 * @property integer $type
 */
class News extends CActiveRecord
{
   
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT     = 0; 
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return News the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title, content, author, created, status', 'required','message'=>'{attribute} не має бути порожнім'),
            array('author, created, status', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>255),
            array('type','numerical','allowEmpty'=>Yii::app()->user->name!=='admin'),
            array('deadline','date','format'=>'dd-mm-yyyy','allowEmpty'=>true),
            array('id, title, content, author, created, status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
           'author_created'=>array(self::BELONGS_TO,'User','author'),
           'category'=>array(self::BELONGS_TO,'Lookup','type') 
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Основний текст',
            'author' => 'Автор',
            'created' => 'Created',
            'status' => 'Статус',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('author',$this->author);
        $criteria->compare('created',$this->created);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function beforeValidate() {
        if ($this->isNewRecord){
            $this->created = time();
            $this->author = Yii::app()->user->id;
        }
        return parent::beforeValidate();
    }
}
