<?php 

class PhotoForm extends CFormModel
{
    public $photo; 
    public $image;
    public function rules()
    {
        return array(
            array('photo','file','maxSize'=>2048000,'minSize'=>100,'types'=>'png,jpeg,jpg,gif')
        );
    }
}
