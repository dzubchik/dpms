<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property string $visible
 * @property string $content
 * @property integer $created
 */
class Pages extends CActiveRecord
{
    const STATUS_VISIBLE = 1;
    const STATUS_HIDE = 0;

    const MENU_NONE = 0;
    const MENU_BEGIN = 1;
    const MENU_END = 2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title, content','required'),
			array('created', 'numerical', 'integerOnly'=>true),
			array('slug', 'length', 'max'=>50),
			array('title', 'length', 'max'=>50),
			array('visible, menu', 'length', 'max'=>1),
			array('content', 'safe'),
            array('visible','default','value'=>self::STATUS_VISIBLE),
            array('menu','default','value'=>self::MENU_NONE),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, slug, title, visible, content, created', 'safe', 'on'=>'search'),
		);
	}

    public function beforeValidate()
    {
        if(strlen($this->slug)<1)
            $this->slug = strtolower(UrlTransliterate::cleanString($this->title,'-',false));
        return parent::beforeValidate();
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'slug' => 'Slug',
			'title' => 'Title',
			'visible' => 'Visible',
			'content' => 'Content',
			'created' => 'Created',
			'menu' => 'Show in menu',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('visible',$this->visible,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getMenu($from = self::MENU_BEGIN)
    {
        $c = new CDbCriteria();
        $c->addCondition("`menu`='$from'");
        $c->addCondition("`visible`='" . Pages::STATUS_VISIBLE . "'");
        $pages = Pages::model()->findAll($c);
        $result = array();
        foreach ($pages as $page) {
            $result[] = array('label' => $page->title, 'url' => '/' . $page->slug, 'active' => Yii::app()->request->getParam('slug') == $page->slug);
        }
//        die(var_dump($result));
        return $result;
    }
}
