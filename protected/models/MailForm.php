<?php

class MailForm extends CFormModel
{
	public $body;
    public $subject;

	public function rules()
	{
		return array(
			array('body,subject', 'required','message'=>'{attribute} не може бути пустим'),
			array('body', 'length', 'min'=>100, 'max'=>4000),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'subject'=>'Тема',
			'body'=>'Текст листа',
		);
	}
}
