<?php

class ResetPasswordForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return array(
            array('email', 'required', 'message' => 'Введіть адресу'),
            array('email', 'email', 'checkMX' => true, 'message' => 'Некоректна адреса'),
            array('email', 'exist', 'className' => 'User', 'attributeName' => 'email', 'allowEmpty' => false,'message' => 'Некоректна адреса'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
        );
    }
}
