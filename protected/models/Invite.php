<?php

/**
 * This is the model class for table "{{invite}}".
 *
 * The followings are the available columns in table '{{invite}}':
 * @property integer $id
 * @property string $email
 * @property string $key
 * @property string $used
 * @property integer $created
 */
class Invite extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Invite the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'invite';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email, key, used, created', 'required'),
            array('created', 'numerical', 'integerOnly'=>true),
            array('email', 'length', 'max'=>50),
            array('email','csvmail'),
            array('key', 'length', 'max'=>32),
            array('used', 'length', 'max'=>1),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, email, key, used, created', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'email' => 'Електронна адреса',
            'key' => 'Key',
            'used' => 'Used',
            'created' => 'Created',
        );
    }

    public function csvmail($attribute)
    {
        $pattern='/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/';
        $emails = explode(',',$this->$attribute);
        foreach($emails as $email){
            if(!preg_match($pattern,trim($email)))
                 $this->addError($attribute, "Адреса <b>".$email."</b> введена некоректно.");
        }
    }
}
