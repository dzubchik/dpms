<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div class="news_view">
	<h2><?php echo CHtml::link(CHtml::encode(strip_tags($data->title)), array('news/view', 'id' => $data->id)); ?></h2>

	<p><?php echo $data->content; ?></p>
	<span class="news_date">Додано: <?php echo date(Yii::app()->params['date'], $data->created); ?>
        <?php if(strlen($data->deadline)>1):?>
        &nbsp; <font color="red">DEADLINE: <?php echo str_replace('-','.',$data->deadline);?></font>
        <?php endif;?>
    </span> <br>
</div>
