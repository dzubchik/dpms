<?php
/* @var $this NewsController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs = array(
	'Випусники'=>array('user/index'),
	'Новини випускників'
);
?>

<?php
#die(var_dump($dataProvider->getData()));
?>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_alumnus',
	'summaryText'=>'&nbsp;',
	'emptyText'=>'Випускники ще нічого не написали.',
	'template'=>"{items}\n{pager}",
)); ?>