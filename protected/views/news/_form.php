<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');?>
<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Усі поля є обов’язковими.</p>

	<?php echo $form->errorSummary($model,'Виправте наступні помилки'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',CHtml::listData(Lookup::model()->findAll(), 'id', 'value')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>
	<div class="row">
   		<?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
   		<label>Deadline</label>
		<?php
    		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    		'language'=>'en',
    		'model'=>$model,
    		'attribute'=>'deadline',
    		'options'=>array(
    		    'showAnim'=>'fold',
    		    'dateFormat'=>'mm-dd-yy',
    		),
   
    	'htmlOptions'=>array(
        'style'=>'height:20px;'
    	),
	)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', array('1'=>'Published','0'=>'Draft')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="form_button">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Додати' : 'Зберегти'); ?>
	</div>

        <?php
            $this->widget('application.lib.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'selector'=>'#News_content',
                'options' => array(
                    'lang' => 'ua',
                    'minHeight'=>'150',
                ),
            ));
        ?>
<?php $this->endWidget(); ?>

</div><!-- form -->
