<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Новини'=>array('index'),
	'Додати',
);
?>

<h1>Додати новину</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
