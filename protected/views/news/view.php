<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs = array(
	'Новини' => array('index'),
	strip_tags($model->title),
);
?>

<div class="user_fullnews">

	<h1><?php echo $model->title; ?>
		<?php if ($model->author != 0): ?>
			<div class="category">[<?php echo $model->category->value; ?>]</div>
		<?php endif; ?>

	</h1>

	<div class="user_fullnews_content"><?php echo $model->content; ?></div>

	<div class="user_fullnews_info">
		<div class="user_fullnews_date"><b><?php echo date(Yii::app()->params['date'], $model->created); ?></b></div>
		<div class="user_fullnews_author">
			<?php if (!is_null($model->author_created)) {
				echo CHtml::link(
					$model->author_created->surname . ' ' . $model->author_created->name,
					array(
						'user/view',
						'id'   => $model->author_created->id,
						'name' => ($model->author_created->surname . '-' . $model->author_created->name))
				);
			}?></div>
	</div>

</div>
