<div class="item_user_news">
	<div class="user_news_title">
		<h3><?php echo CHtml::link ($data->title, array('/news/view', 'id' => $data->id)); ?></h3>

		<div class="user_news_edit">
			<?php if (Yii::app ()->user->id == $data->author_created->id): ?>
				<?php echo CHtml::link ('Редагувати', array('news/update', 'id' => $data->id)); ?>
			<?php endif; ?>
		</div>
	</div>

	<div class="category">[<?php echo $data->category->value; ?>]</div>

	<div class="user_news_text">
		<?php
			echo $data->content;
		?>
	</div>
	<div class="user_news_author">
		<?php echo CHtml::link ($data->author_created->surname . ' ' . $data->author_created->name, array('user/view', 'id' => $data->author_created->id,'name'=>($data->author_created->surname.'-'.$data->author_created->name))); ?>
		<b><?php echo date (Yii::app ()->params['date'], $data->created); ?></b>
	</div>

</div>
