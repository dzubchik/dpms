<?php
/* @var $this NewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Новини',
);
?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider' => $dataProvider,
	'ajaxUpdate'   => false,
	'itemView'     => '_view',
	'emptyText'    => 'Новини відсутні.',
	'pager'        => array(
		'class'          => 'CLinkPager',
		'firstPageLabel' => '<<',
		'prevPageLabel'  => 'Назад',
		'nextPageLabel'  => 'Вперед',
		'lastPageLabel'  => '>>',
		'header'=>'На сторінку:'
	),
)); ?>
