<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'News'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('news-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage News</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		array(
			'name'=>'content',
			'value'=>'wordwrap(strip_tags($data->content),100,"<br />",true)',
			'type'=>'raw'
		),
		array(
			'name'=>'author',
			'value'=>'($data->author==0) ? "admin":$data->author_created->full_name'
		),
		array(
			'name'=>'created',
			'value'=>'date(Yii::app()->params[\'date\'],$data->created)'
		),
		array(
			'name'=>'status',
			'value'=>'($data->status==News::STATUS_PUBLISHED) ? "Опубліковано":"Чорновик"'
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
