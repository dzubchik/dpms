<?php
/* @var $this SiteController */
/* @var $model ResetPasswordForm */
/* @var $form CActiveForm  */
?>
<center>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="error success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::app()->user->hasFlash('fail')): ?>
        <div class="error fail">
            <?php echo Yii::app()->user->getFlash('fail'); ?>
        </div>
    <?php endif; ?>
    <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'reset-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email'); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>

        <div class="separator"></div>
        <div class="form_button">
            <?php echo CHtml::submitButton('Скинути пароль'); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div><!-- form -->
</center>
