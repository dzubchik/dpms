<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
?>
<center>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
    <?php echo CHtml::link('Забулись пароль?',array('site/resetpassword'),array('style'=>'color: #1e9026;'));?>
    <br><br>
<div class="separator"></div>
	<div class="form_button">
		<?php echo CHtml::submitButton('Вхід'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</center>
