<?php
/* @var $this InviteController */
/* @var $model Invite */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'invite-form',
	'enableAjaxValidation'=>false,
)); ?>
    <?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="error success">
        <?php  echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif;?>

    <?php if(Yii::app()->user->hasFlash('fail')):?>
    <div class="error fail">
        <?php  echo Yii::app()->user->getFlash('fail'); ?>
    </div>
    <?php endif;?>

	<?php echo $form->errorSummary($model); ?>
	<div class="invite_form">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
        <p class="hint">Можете вводити декілька адрес через кому</p> 
		<?php echo $form->error($model,'email'); ?>
	</div>
<div class="separator"></div>
	<div class="form_button">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Запросити' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
