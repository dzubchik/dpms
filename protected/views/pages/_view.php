<?php
/* @var $this PagesController */
/* @var $data Pages */
?>
<div class="about_page">
    <div class="about_text">
        <?php echo CHtml::encode($data->content); ?>
    </div>
</div>