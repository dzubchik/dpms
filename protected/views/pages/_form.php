<?php
/* @var $this PagesController */
/* @var $model Pages */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pages-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'slug'); ?>
		<?php echo $form->textField($model,'slug',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'slug'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>



	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'visible'); ?>
        <?php echo $form->dropDownList($model,'visible', array(Pages::STATUS_VISIBLE=>'Published',Pages::STATUS_HIDE=>'Draft')); ?>
        <?php echo $form->error($model,'visible'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'menu'); ?>
        <?php echo $form->dropDownList($model,'menu', array(Pages::MENU_NONE=>'No',Pages::MENU_BEGIN=>'In the begin', Pages::MENU_END =>'In the end')); ?>
        <?php echo $form->error($model,'menu'); ?>
    </div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
    <?php
    $this->widget('application.lib.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
        'selector'=>'#Pages_content',
        'options' => array(
            'lang' => 'ua',
            'minHeight'=>'150',
        ),
    ));
    ?>
<?php $this->endWidget(); ?>

</div><!-- form -->