<?php
/* @var $this PagesController */
/* @var $model Pages */

$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Manage',
);

?>
<h1>Manage Pages</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pages-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'slug',
		'title',
        array(
            'name'=>'menu',
            'header'=>'В меню?',
            'value'=>'($data->menu==Pages::MENU_NONE) ? "Ні":"Так"'
        ),
        array(
		    'name'=>'visible',
            'header'=>'Статус',
            'value'=>'($data->visible==Pages::STATUS_VISIBLE) ? "Опубліковано":"Чорновик"'
        ),
		array(
            'name'=>'content',
            'value'=>'strip_tags($data->content)',
            'type'=>'raw'
        ),
		array(
			'class'=>'CButtonColumn',
            'buttons'=>array(
                'view' => array(
                    'url'=>'Yii::app()->createUrl("$data->slug")',       //A PHP expression for generating the URL of the button.
                )
            )
		),
	),
)); ?>
