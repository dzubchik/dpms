<?php
/* @var $this PagesController */
/* @var $model Pages */

$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Create',
);
?>

<h1>Додати сторінку</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>