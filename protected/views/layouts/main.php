<?php /* @var $this Controller */ ?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.less">
	<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.6.2/less.min.js" type="text/javascript"></script>
	<link
		href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAFo9M/3AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAhhJREFUeNpiLDrgwgACTAwQ8B8ggBiBIlOBDBEWIJENEgIIIJDIaiC9C4hnw9TyQjlsIAGAAAKpaALSdUAcClLJBOVoAfEsIHYHGfYfakUFECuBVDAC8Rsg1gDi0wABBDJjIpCRD1UVA8SLoYpAYCNIRykQBwOxKhAHMCDAFyAWBtnhD8Qgt74AYgkgToAqWAvEQiAT9gLxFiCuBeJ1QCwJxGuAeCsQ+wAEECM0NJYC8R4gng/VPRmImYE4C2TFEyCWgYUWELcDcS6UvwNkxUIo5xfU9c1A3AYV+w4ywQ2I46BhBzLhMhDbAnEEyF0s0DC4BsQmUF26QPwRiJeDTARZMQOIk4D4DDQ8QNacgtIdTNCQ6wPiGqii60BsBjVNnwXqoOdArAnEyVA3pIMkgbgBIMBA4cAKZNwE4h9AvAka4epQE/qB+CvU26JAfAuIH0I9Zg3EKiAbDkCdMAUpnlyBeCrUnV+g6WMSEE+HyoOCygGI5zBB4ywKKgGKln9Qm9SA+BMQi0GdD9L8GinlgALmAQvU9sfQMLaCplsDkCQDKiiCekMaKUarmJBMA/l3ARCDDL0AimYkzXeh6QMEdkLVmoJSGwtU0yOoDUVQvwYBsQIQ/0Ey5AzUVTpQfhoo3zBBFVkiKcyDJr5oqFNBuAuaUkKQ1JnAMkQKiAHEi4CYB5QAgfgpNFfCshbI7y1ALAdVAwKNQHwRANzVcuxoxnr4AAAAAElFTkSuQmCC"
		rel="icon" type="image/x-icon"/>
	<title><?php echo CHtml::encode($this->pageTitle) . " | " . Yii::app()->name; ?></title>

</head>

<body>

<div class="container" id="page">

	<div class="header">
		<a href="/">
			<div class="logo"></div>
		</a>

		<div class="title"><!-- <?php echo CHtml::encode(Yii::app()->name); ?>-->
			<h3>Національний університет «<a href="http://ukma.edu.ua/" title="Сайт університету">Києво-Могилянська
					Академія</a>»</h3>

			<h2>Випускники кафедри фізико-математичних наук</h2>
		</div>
		<div class="social">
			<div class="linkedin">
				<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
				<script type="IN/Share" data-counter="right"></script>
			</div>
			<div class="twitter">
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="uk">Твіт</a>
				<script>!function (d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (!d.getElementById(id)) {
							js = d.createElement(s);
							js.id = id;
							js.src = "//platform.twitter.com/widgets.js";
							fjs.parentNode.insertBefore(js, fjs);
						}
					}(document, "script", "twitter-wjs");</script>
			</div>

		</div>
	</div>
	<!-- header -->

	<div class="mainmenu">
		<?php
        $this->widget('zii.widgets.CMenu', array(
            'items' => CMap::mergeArray(
                    Pages::getMenu(),
                    array(
                        array('label' => 'Випускники', 'url' => array('/user/index')),
                        array('label' => 'Новини', 'url' => array('/news/index')),
                        array('label' => 'Профіль', 'url' => array('/user/view', 'id' => Yii::app()->user->id), 'visible' => !Yii::app()->user->isGuest && Yii::app()->user->name !== 'admin'
                        ),
                        array('label' => 'Вхід', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Вихід', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                    ),
                    Pages::getMenu(Pages::MENU_END)
                )
        )); ?>
	</div>
	<!-- mainmenu -->

	<div class="navigation">
		<?php if (isset($this->breadcrumbs)): ?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
		<?php endif ?>



	</div>
    <?php if (!Yii::app()->user->isGuest && Yii::app()->user->name == 'admin'): ?>
        <!--begin block for admin -->
        <div class="administration">
            <div class="form_button"><?php echo CHtml::link('Додати новину', array('/news/create')); ?></div>
            <div class="form_button"><?php echo CHtml::link('Керування новинами', array('/news/admin')); ?></div>
            <div class="form_button"><?php echo CHtml::link('Надіслати запрошення', array('/invite/create')); ?></div>
            <div class="form_button"><?php echo CHtml::link('Розсилка', array('/user/mail')); ?></div>
            <div class="form_button"><?php echo CHtml::link('Додати сторінку', array('/pages/create')); ?></div>
            <div class="form_button"><?php echo CHtml::link('Керування сторінками', array('/pages/admin')); ?></div>
        </div>
        <br><br>
        <!--end block for admin-->
    <?php endif; ?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div class="footer">

		<a href="http://usic.at" title="Розробники сайту — команда Студентського Інтернет центру НаУМКА">
			<div class="usic_logo"></div>
		</a>

	</div>
	<!-- footer -->

</div>
<!-- page -->
<a href="#" class="scrollup" title="Вгору" style="display: inline;">
	<div class="scroll_img">&#x21e7;</div>
</a>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter20076403 = new Ya.Metrika({id:20076403, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/20076403" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</body>
</html>
