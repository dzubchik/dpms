<div class="form">
	<?php Yii::import('application.lib.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'                     => 'mail-form',
		'enableClientValidation' => true,
		'clientOptions'          => array(
			'validateOnSubmit' => true,
		),
	)); ?>
	<?php if (Yii::app()->user->hasFlash('success')): ?>
		<div class="error success">
			<?php echo Yii::app()->user->getFlash('success'); ?>
		</div>
	<?php endif; ?>

	<?php if (Yii::app()->user->hasFlash('fail')): ?>
		<div class="error fail">
			<?php echo Yii::app()->user->getFlash('fail'); ?>
		</div>
	<?php endif; ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'subject'); ?>
		<?php echo $form->textField($model, 'subject'); ?>
		<?php echo $form->error($model, 'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'body'); ?>
		<?php echo $form->textArea($model, 'body', array('cols' => 60, 'rows' => 20)); ?>
		<?php echo $form->error($model, 'body'); ?>
	</div>

	<?php
	$this->widget('ImperaviRedactorWidget', array(
		'selector' => '#MailForm_body',
		'options'  => array(
			'lang'      => 'ua',
			'minHeight' => '150',
			'fixed'     => false,
			'fixedBox'  => false,
		),
	)); ?>
	<div class="form_button">
		<?php echo CHtml::submitButton('Розіслати'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->
