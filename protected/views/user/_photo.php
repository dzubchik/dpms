<?php
$this->breadcrumbs=array(
	'Випускники'=>array('index'),
	$model->surname.' '.$model->name=>array('view','id'=>$model->id),
	'Оновлення'=>array('update','id'=>$model->id),
    'Фоточка'
);?>
<div class="form">
    <?php if(Yii::app()->user->hasFlash('img_good')):?>
    <div class="error success">
        <?php  echo Yii::app()->user->getFlash('img_good'); ?>
    </div>
    <?php endif;?>

    <?php if(Yii::app()->user->hasFlash('img_bad')):?>
    <div class="error fail">
        <?php  echo Yii::app()->user->getFlash('img_bad'); ?>
    </div>
    <?php endif;?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),

));?>
	<?php echo $form->errorSummary($photo); ?>

	<div>
		<?php echo $form->labelEx($photo,'photo'); ?>
		<?php echo $form->fileField($photo,'photo'); ?>
	<div class="form_button">
		<?php echo CHtml::submitButton(strlen($model->photo)==Yii::app()->params['photoLength']?'Змінити фото':'Додати фото'); ?>
	</div>
<?php $this->endWidget(); ?>
</div>
