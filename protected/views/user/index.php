<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Випускники',
);
?>

<div class="user_countainer">
	<div class="random_users">
		<div id="filter">
			<ul>
				<?php foreach ($years as $year): ?>
					<li><?php echo CHtml::link($year['year'], array('user/index', '#' => $year['year'])); ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="items">
			<?php foreach ($years as $year): ?>
				<div class="users_year"><a name="<?php echo $year['year']; ?>"><?php echo $year['year']; ?></a></div>
				<?php foreach ($users[$year['year']] as $user): ?>
					<a href="<?php echo Yii::app()->createUrl('user/view', array('id' => $user['id'], 'name' => ($user['surname'] . '-' . $user['name']))); ?>">
						<div class="user_shortview">
							<div class="user_photo">
								<img src="<?php echo User::getPhoto($user['id'], true);?>">
							</div>
							<div class="user_name">
								<?php echo $user['surname'] . '<br/>' . $user['name']; ?>
							</div>
							<div class="user_endyear">
								<?php echo CHtml::encode($user['enter'] . ' / ' . $user['year']); ?>
							</div>
						</div>
					</a>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</div>
	</div>
</div>


