<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

Yii::import('application.lib.yiiext.imperavi-redactor-widget.*');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля позначені зірочкою є обов'язковими для заповнення.</p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
        <p class="hint">Емейл використовується у якості логіну та ніде не відображається.</p> 
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div>
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
<div class="separator"></div>
<div class="username_forms">
	<div>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'surname'); ?>
		<?php echo $form->textField($model,'surname',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'middle_name'); ?>
		<?php echo $form->textField($model,'middle_name',array('size'=>20,'maxlength'=>20)); ?>
	</div>
</div>
<div class="separator"></div>
<div class="level">
	<label class="required">Вкажіть свій вчений ступінь (найвищий)
<span class="required">*</span>
	</label>
    <div>
    <?php
        echo $form->radioButtonList($model,'level',
          User::getLevels(),array('separator'=>'</div><div>'));
    ?>
	</div>
</div>
<div class="separator"></div>
	<div class="years">
		<?php echo $form->labelEx($model,'enter'); ?>
		<?php echo $form->dropDownList($model,'enter',array_combine(range(1991,date('Y')),range(1991,date('Y')))); ?>
		<?php echo $form->error($model,'enter'); ?>
	</div>
	<div class="years">
		<?php echo $form->labelEx($model,'year'); ?>
		<?php echo $form->dropDownList($model,'year',array_combine(range(1996,date('Y')),range(1996,date('Y')))); ?>
		<?php echo $form->error($model,'year'); ?>
	</div>
<div class="separator"></div>

<div class="detail_info">
	<div>
		<?php echo $form->labelEx($model,'info'); ?>
		<?php echo $form->textArea($model,'info',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'info'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'reason'); ?>
		<?php echo $form->textArea($model,'reason',array('rows'=>6, 'cols'=>50)); ?>
        <?php
            $this->widget('ImperaviRedactorWidget', array(
                'selector'=>'#User_reason,#User_info,#User_publications,#User_additional',
                'options' => array(
                    'lang' => 'ua',
                    'minHeight'=>'150',
#                    'air'=>true,
                    'fixed'=>false,
                    'fixedBox'=>false,
                ),
        ));?>
		<?php echo $form->error($model,'reason'); ?>
	</div>
	<div>
		<?php echo $form->labelEx($model,'publications'); ?>
		<?php echo $form->textArea($model,'publications',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'publications'); ?>
	</div>

	<div class="additional_info">
		<?php echo $form->labelEx($model,'additional'); ?>
		<?php echo $form->textArea($model,'additional',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'additional'); ?>
	</div>

</div>

<div class="separator"></div>

<div class="final_info">
	<div>
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url'); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>
	<div>
		<?php echo $form->labelEx($model,'university'); ?>
		<?php echo $form->textField($model,'university',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'university'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'wish'); ?>
		<?php echo $form->textField($model,'wish',array('size'=>60)); ?>
		<?php echo $form->error($model,'wish'); ?>
	</div>
    
    <div>
    <?php if(Yii::app()->controller->action->id == 'update'):?>
        <?php echo CHtml::link(strlen($model->photo) == Yii::app()->params['photoLength'] ? 'Змінити фото':'Додати фото', array('user/photo'));?>
    </div>
    <?php if(strlen($model->photo) == Yii::app()->params['photoLength']):?>
       <?php echo CHtml::link('Видалити фоточку',"#", array("submit"=>array('photodel'), 'confirm' => 'Точно хочете повернути стандартну унилу фоточку?')); ?> 
    <?php endif;?>
    <?php else:?>
        Завантажити фото можна буде після реєстрації. <br/>Редагувати свій профіль Ви зможете в будь-який момент залогінившись на сайті.
    <?php endif;?>
</div>

<div class="separator"></div>
	<div class="form_button">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Зареєструватись' : 'Зберегти'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->
