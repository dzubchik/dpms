<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Випускники'=>array('index'),
	$model->surname.' '.$model->name=>array('view','id'=>$model->id),
	'Оновлення',
);

?>

<h1>Редагування сторінки користувача: <?php echo $model->surname.' '.$model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'years'=>$years)); ?>
