<?php
/* @var $this UserController */
/* @var $model User */


$this->breadcrumbs = array(
	'Випускники' => array('index'),
	$model->full_name
);
?>
<?php if (Yii::app()->user->hasFlash('info')): ?>
	<div class="info">
		<?php echo Yii::app()->user->getFlash('info'); ?>
	</div>
<?php endif; ?>
<h1 class="user"><span>Випускник:</span> <?php echo $model->surname . ' ' . $model->name . ' ' . $model->middle_name; ?>
</h1>

<div class="user_control_buttons">
	<?php if ($model->id == Yii::app()->user->id): ?>
		<div><?php echo CHtml::link('Редагувати', array('user/update', 'id' => Yii::app()->user->id)); ?></div>
		<div><?php echo CHtml::link('Додати новину', array('news/create')); ?></div>
	<?php endif; ?>
</div>

<div class="user_container">

	<div class="user_leftpanel">

		<div class="user_fullphoto">
			<img
				src="<?php echo User::getPhoto($model->id);?>">
		</div>

		<?php if (strlen($model->university) > 1): ?>
			<div class="user_workplace">
				<?php echo $model->university; ?>
			</div>
		<?php endif; ?>

		<?php if (strlen($model->url) > 1): ?>
			<div class="user_personal_page">
				<a href="<?php echo $model->url; ?>" title="Персональна сторінка"><?php echo $model->url; ?></a>
			</div>
		<?php endif; ?>
		<div class="user_years">
			<span title="Рік вступу"><?php echo $model->enter; ?></span>
			<span title="Рік випуску"><?php echo $model->year; ?></span>
		</div>

		<div class="user_level">
			<?php
			//begin rendering educational level
			$a = User::getLevels();
			if (isset($a[$model->level])) {
				echo $a[$model->level];
			} else {
				echo "Бакалавр";
			} //default value, if it haven't been checked
			//end rendering
			?>
		</div>

	</div>

	<div class="user_rightpanel">

		<?php if (strlen($model->info) > 1): ?>
			<div class="user_cv">
				<h3 class="user_block_title">CV</h3>
				<?php echo $model->info; ?>
			</div>
			<span class="divider"></span>
		<?php endif; ?>

		<?php if (strlen($model->publications) > 1): ?>
			<div class="user_publication">
				<h3 class="user_block_title">Публікації</h3>
				<?php echo $model->publications; ?>
			</div>
			<span class="divider"></span>
		<?php endif; ?>

		<?php if (strlen($model->wish) > 1): ?>
			<div class="user_advice">
				<h3 class="user_block_title">Порада абітурієнтам</h3>

				<div class="user_advice_text"><?php echo $model->wish; ?></div>
			</div>
			<span class="divider"></span>
		<?php endif; ?>

		<?php if (strlen($model->reason) > 1): ?>
			<div class="user_advice">
				<h3 class="user_block_title">Чому варто навчатись на фіз-мат кафедрі НаУКМА</h3>

				<div class="user_advice_text"><?php echo $model->reason; ?></div>
			</div>
			<span class="divider"></span>
		<?php endif; ?>

		<?php if (strlen($model->additional) > 1): ?>
			<div class="user_additional">
				<h3 class="user_block_title">Хоббі, додаткова інформація</h3>

				<div class="user_additional"><?php echo $model->additional; ?></div>
			</div>
		<?php endif; ?>

	</div>
</div>
