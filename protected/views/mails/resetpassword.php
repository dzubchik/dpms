<div style="background: #f7f7f7; padding: 20px; margin: 0 auto; color: #000000; font: 1em Helvetica, Tahoma;">

    <div style="width: 720px; border: 1px solid #c5c5c5; margin: 0 auto; padding: 20px; background: #ffffff;">
        <div style="">
            <img src="http://usic.at/~robert/static/physics_logo.png">
            <div style="display: inline-block; vertical-align: top; margin: 5px 0 0 20px;">
                <h2 style="font-weight: normal; color: #c5c5c5;">Національний університет «Києво-Могилянська Академія»</h2>
                <h1 style="font-weight: normal;">Кафедра фізико-математичних наук</h1>
            </div>
        </div>

        <h2>Зміна паролю на сайті випускників кафедри фіз.-мат. наук НаУКМА</h2>
        <h4 style="font-weight: normal; line-height: 24px;">Ваш новий пароль:<b><?php echo $password;?></b>. Ви можете його змінити у налаштуваннях профілю.</h4>

    </div>

</div>
