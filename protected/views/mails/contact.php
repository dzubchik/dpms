<div style="background: #f7f7f7; padding: 20px; margin: 0 auto; color: #000000; font: 1em Helvetica, Tahoma;">

	<div style="width: 720px; border: 1px solid #c5c5c5; margin: 0 auto; padding: 20px; background: #ffffff;">
		<div style="">
			<img src="http://usic.at/~robert/static/physics_logo.png">
			<div style="display: inline-block; vertical-align: top; margin: 5px 0 0 20px;">
				<h2 style="font-weight: normal; color: #c5c5c5;">Національний університет «Києво-Могилянська Академія»</h2>
				<h1 style="font-weight: normal;">Кафедра фізико-математичних наук</h1>
			</div>
		</div>

		<div style="font-size: 1.2em;">
		<p style="text-align: center; margin: 20px 0 40px 0; line-height: 22px;">Запрошуємо Вас до реєстрації на сайті кафедри. <br><br>Сайт створено для надання вичерпних відповідей на питання абітурієнтів, забезпечення корисною інформацією студентів та зв'язку з випускниками.</p>
		<h3 style="margin: 20px 0 20px 20px;">Зареєструвавшись, Ви отримаєте можливість:</h3>
		<ul style="font-size: 1em; line-height: 24px;">
			<li>Розказати про себе та поділитися досвідом;</li>
			<li>Розповісти, що Вам дала Могилянка та кафедра фізики, або фіз.-мат. освіта загалом;</li>
			<li>Перелічити свої публікації, поділитись своїми здобутками – якщо Ви займаєтесь наукою, – або розказати про те, в яких галузях і як Ви будуєте Вашу кар’єру;</li>
			<li>Залишити контактну інформацію та пораду майбутнім фізикам :)</li>
		</ul>
		<h3 style="margin: 20px 0 20px 20px;">В майбутньому за допомогою сайту Ви зможете:</h3>
		<ul style="font-size: 1em; line-height: 24px;">
			<li>Розміщувати новини, анонси подій, інформацію про відкриті вакансії;</li>
			<li>Бути в курсі того, що відбувається на кафедрі, та пропонувати свої ідеї для її розвитку.</li>
        </ul>
		</div>

		<div style="width: 90%; height: 1px; background: #c5c5c5; margin: 20px auto 20px auto;"></div>

			<p style="text-align: center;">Перейти до процедури реєстрації Ви можете за посиланням, розміщеним нижче:</p>

		<div style="color: #49a4ff; font-size: 1.4em; text-align: center;">
		<?php echo CHtml::link('Зареєструватись',$this->createAbsoluteUrl('user/create',array('key'=>$key)));?>
		</div>

	</div>

</div>
