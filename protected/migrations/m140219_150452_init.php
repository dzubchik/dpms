<?php

class m140219_150452_init extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('invite', array(
			'id'      => 'pk',
			'email'   => 'varchar(50)',
			'key'     => 'varchar(32)',
			'used'    => 'ENUM("0","1")',
			'created' => 'int(11)'
		));

		$this->createTable('lookup', array(
			'id'    => 'pk',
			'type'  => 'varchar(255)',
			'value' => 'varchar(255)',
		));

		$this->createTable('news', array(
			'id'       => 'pk',
			'title'    => 'varchar(255)',
			'content'  => 'longtext',
			'author'   => 'int(5)',
			'created'  => 'int(11)',
			'status'   => 'int(3)',
			'deadline' => 'varchar(20)',
			'type'     => 'int(5)'
		));

		$this->createTable('users', array(
			'id'           => 'pk',
			'email'        => 'varchar(50)',
			'name'         => 'varchar(20)',
			'surname'      => 'varchar(20)',
			'middle_name'  => 'varchar(20)',
			'password'     => 'varchar(128)',
			'photo'        => 'varchar(32)',
			'info'         => 'longtext',
			'reason'       => 'longtext',
			'publications' => 'longtext',
			'additional'   => 'longtext',
			'wish'         => 'longtext',
			'year'         => 'int(4)',
			'enter'        => 'int(4)',
			'level'        => 'ENUM("0","1","2")',
			'university'   => 'varchar(255)',
			'url'          => 'varchar(50)',
		));

	}

	public function safeDown()
	{
		$this->dropTable('invite');
		$this->dropTable('lookup');
		$this->dropTable('news');
		$this->dropTable('users');
	}
}