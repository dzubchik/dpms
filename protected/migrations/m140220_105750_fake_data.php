<?php

require_once dirname(__FILE__) . '/../lib/fzaninotto/faker/src/autoload.php';

class m140220_105750_fake_data extends CDbMigration
{
	public function safeUp()
	{
		$faker = Faker\Factory::create('uk_UA');
		for ($i = 0; $i < 100; $i++) {
			$year = rand(1996, 2013);
			$level = rand(0, 2);
			$this->insert('users', array(
				'email'        => $faker->email,
				'name'         => $faker->firstName,
				'surname'      => $faker->lastName,
				'middle_name'  => $faker->middleName,
				'password'     => hash('sha512', 'password'),
				'info'         => $faker->text(500),
				'reason'       => implode(' ', $faker->words(20)),
				'publications' => '<p>' . implode('</p><p>', $faker->paragraphs(5)) . '</p>',
				'additional'   => $faker->paragraph(2),
				'wish'         => implode(' ', $faker->words(8)),
				'year'         => $year,
				'enter'        => $year - ($level == User::LEVEL_BACHELOR ? 4 : ($level == User::LEVEL_MASTER ? $faker->randomElement(array(2, 6)) : 1)),
				'level'        => $level,
				'university'   => 'Інститут ' . $faker->word,
				'url'          => $faker->url
			));
		}
	}

	public function safeDown()
	{
		$this->truncateTable('users');
	}
}