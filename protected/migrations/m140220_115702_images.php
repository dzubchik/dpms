<?php

class m140220_115702_images extends CDbMigration
{
	public function safeUp()
	{
		Yii::app()->db->createCommand("UPDATE users set photo=concat('',photo,'.jpg')")->query();
	}

	public function safeDown()
	{
		echo 'This migration does not support downgrade';
		return -1;
	}
}