<?php
return array(
	'defaultController' => 'news/index',
//	'components'        => array(
//		'db'           => array(
//			'password' => 'DBPASSWORD',
//		),
//		'sentry'       => array(
//			'class'             => 'application.lib.crisu83.yii-sentry.components.SentryClient',
//			'dns'               => 'SENTRYDNS',
//			'environment'       => 'production',
//			'yiiExtensionAlias' => 'application.lib',
//			'dependencies'      => array(
//				'raven'         => 'application.lib.raven.raven',
//				'yii-extension' => 'application.lib.crisu83.yii-extension',
//			)
//		),
//		'errorHandler' => array(
//			'class' => 'application.lib.crisu83.yii-sentry.components.SentryErrorHandler',
//		),
//		'log'          => array(
//			'class'  => 'CLogRouter',
//			'routes' => array(
//				array(
//					'class'  => 'application.lib.crisu83.yii-sentry.components.SentryLogRoute',
//					'levels' => 'error, warning',
//				),
//			),
//		),
//	),
	'params'            => array(
		'adminEmail' => 'dmytro@dzubenko.name',
		'mail'       => array('no-reply@physics.ukma.edu.ua' => 'Кафедра фізико-математичних наук НАУКМА'),
	)
);