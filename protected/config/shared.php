<?php

return array(
	'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'              => 'Кафедра фізико-математичних наук НАУКМА',
	'preload'           => array('log'),
	'import'            => array(
		'application.models.*',
		'application.components.*',
		'application.lib.mail.*',
        'application.lib.transliterate.*',
		'ext.*'
	),
   'components'        => array(
		'user'         => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),
		'mail'         => array(
			'class'     => 'Mailer',
			'transport' => array(
				'class' => 'Swift_MailTransport',
			),
		),

		// uncomment the following to enable URLs in path-format
		'urlManager'   => array(
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'caseSensitive'  => false,
			'rules'          => array(
				'<action:(login|logout|resetpassword)>'  => 'site/<action>',
				'<controller:(news|user)>'              => '<controller>/index',
				'user/<id:\d+>-<name:.+>'                => 'user/view',
				'<controller:\w+>/<id:\d+>'              => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
                '<slug:.+>'                              => 'pages/view',
			),
		),
		'db'           => array(
			'connectionString' => 'mysql:host=localhost;dbname=physics',
			'emulatePrepare'   => true,
			'username'         => 'physics',
			'password'         => 'DBPASSWORD',
			'charset'          => 'utf8',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),

	'params'            => array(
		'adminEmail'     => 'admin@localhost',
		'mail'           => array('no-reply@localhost' => 'Кафедра фіз-мат наук НАУКМА'),
		'date'           => 'j.m.y H:i',
		'uploadFolder'   => '/img/',
		'uploadFolderTh' => '/img/t/',
	),
);
