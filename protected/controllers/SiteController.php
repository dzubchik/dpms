<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $this->setPageTitle("Вхід");
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if ($model->username == 'admin') {
                    $this->redirect(Yii::app()->createUrl('invite/create'));
                }
                $this->redirect(Yii::app()->createUrl('user/view', array('id' => Yii::app()->user->id)));
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionResetPassword()
    {
        $this->setPageTitle("Відновлення паролю");
        $model = new ResetPasswordForm();
        if (isset($_POST['ResetPasswordForm'])) {
            $model->attributes = $_POST['ResetPasswordForm'];
            if ($model->validate()) {
                $newpassword = $this->_randomPassword(10);
                $hash = hash('sha512', $newpassword);
                $user = User::model()->findByAttributes(array('email' => $model->email));
                $user->password = $hash;
                $user->save();
                $mailer = Yii::app()->mail;
                $mail = $mailer->newMessage(array(
                    'subject' => 'Зміна паролю на сайті випускників кафедри фіз.-мат. наук НаУКМА',
                    'from' => Yii::app()->params['mail'],
                    'to' => $model->email,
                ));
                $mail->view = array('resetpassword', 'password' => $newpassword);
                if (!$mailer->send($mail)) {
                    Yii::app()->user->setFlash('fail', 'Помилка відправки повідомлення. Спробуйте пізніше');
                }
                else{
                    Yii::app()->user->setFlash('success', 'На Вашу пошту відправлено новий пароль');
                }
            }
        }
        $this->render('resetpassword', array(
            'model' => $model
        ));
    }

    private function _randomPassword($length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}
