<?php

class InviteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Invite;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invite']))
		{
            $model->attributes  = $_POST['Invite'];
            if($model->validate(array('email')))
            {
                $emails = explode(',',$model->email);
                array_unique($emails);
                foreach($emails as $email){
                    $email = trim($email);
                    $model = new Invite;
                    $key = md5(time().$email);
                    $model->attributes = array('key'=>$key,'created'=>time(),'used'=>0,'email'=>$email);
                    $model->save(false); 

                    $mailer = Yii::app()->mail;
                    $mail = $mailer->newMessage(array(
                        'subject' => 'Запрошення на реєстрацію',
                        'from'=>Yii::app()->params['mail'],
                        'to' => $model->email, 
                    ));
                    $mail->view = array('contact','key'=>$model->key);
                    if(!$mailer->send($mail)){
                        Yii::app()->user->setFlash('fail','Запрошення не може бути відправлено. Спробуйте пізніше.');
                    }
                }
                if(!Yii::app()->user->getFlash('fail')){
                    Yii::app()->user->setFlash('success','Запрошення були успішно відправлені.');             
                }
				$this->refresh();
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Invite');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Invite('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invite']))
			$model->attributes=$_GET['Invite'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Invite::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invite-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
