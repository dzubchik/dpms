<?php

class UserController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete, photodel', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow all users to perform 'index' and 'view' actions
				'actions' => array('create', 'index', 'view'),
				'users'   => array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('update', 'photo', 'photodel'),
				'users'   => array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete', 'mail'),
				'users'   => array('admin'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->setPageTitle('Профіль користувача');
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($key)
	{
		$this->setPageTitle('Реєстрація користувача');
		if (!Yii::app()->user->isGuest) {
			Yii::app()->user->logout(); // logout user to be registreted
			$this->refresh();
		}
		if (strlen($key) != 32) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		$invite = Invite::model()->find('`key`=:key', array(':key' => $key));
		if ($invite === null || $invite->key !== $key) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		if ($invite->used == 1) {
			throw new CHttpException(404, "Вибачте, але це запрошення вже було використано.");
		}

		$model = new User;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			if ($model->validate()) {
				$model->password = hash('sha512', $model->password);
				$model->save(false);
				$invite->used = 1;
				$invite->save();
				Yii::app()->user->setFlash('info', 'Щиро дякуємо за реєстрацію. Як тільки сайт запрацює на повну потужність, ми Вас обов’язково повідомимо листом.');
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$model->email = $invite->email;
		for ($i = 1992; $i < date('Y'); $i++) {
			$years[$i] = $i;
		}
		$this->render('create', array(
			'model' => $model,
			'years' => $years
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->setPageTitle('Редагування профілю');
		$model = $this->loadModel($id);
		if (Yii::app()->user->getState('email') !== $model->email && Yii::app()->user->name !== 'admin') {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		$this->performAjaxValidation($model);
		if (isset($_POST['User'])) {
			$pass = $_POST['User']['password'];
			$old_pass = $model->password;

			$model->attributes = $_POST['User'];
			$model->password = $old_pass;
			if (strlen($pass) >= 8 && $old_pass !== hash('sha512', $pass)) {
				$model->password = hash('sha512', $pass);
			}
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		for ($i = 1992; $i < date('Y'); $i++) {
			$years[$i] = $i;
		}
		$model->password = '';
		$this->render('update', array(
			'model' => $model,
			'years' => $years
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->setPageTitle('Випускники');
		$c = Yii::app()->db->createCommand();
		$users = $c->select()->from('users')->order('enter ASC,surname')->queryAll();
		foreach ($users as $u) {
			$users[$u['year']][] = $u;
		}
		$c->reset();
		$years = $c->select('year as year')->from('users')->group('year')->order('year DESC')->queryAll();
		$this->render('index', array(
			'users' => $users,
			'years' => $years
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new User('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['User'])) {
			$model->attributes = $_GET['User'];
		}

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionPhoto()
	{
		$this->setPageTitle('Редагування фото');
		$model = new PhotoForm;
		if (isset($_POST['PhotoForm'])) {
			$model->attributes = $_POST['PhotoForm'];
			if ($model->validate()) {
				$model->image = EUploadedImage::getInstance($model, 'photo');
				$model->image->maxWidth = 240;
				$model->image->maxHeight = 320;
				$model->image->thumb = array(
					'maxWidth'  => 64,
					'maxHeight' => 64,
					'dir'       => 't',
					'prefix'    => 't',
				);
				$name = substr(md5(time() . Yii::app()->user->name), 0, Yii::app()->params['photoLength']).'.jpg'; // name of file
				if ($model->image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['uploadFolder'] . $name)) {
					$this->setPhoto($name);
					Yii::app()->user->setFlash('img_good', 'Зображення було успішно змінено.');
				} else {
					Yii::app()->user->setFlash('img_bad', 'Вибачте, але сталася помилка. Спробуйте пізніше.');
				}
			}
		}
		$this->render('_photo', array(
			'photo' => $model,
			'model' => $this->loadModel(Yii::app()->user->id)
		));
	}

	public function actionPhotoDel()
	{
		$user = User::model()->findByPk(Yii::app()->user->id);
		if (!is_null($user) && $user !== false && strlen($user->photo) == Yii::app()->params['photoLength']) { // sorry for dogs
			@unlink(Yii::getpathOfAlias('webroot') . Yii::app()->params['uploadFolder'] . $user->photo . '.jpg');
			@unlink(Yii::getpathOfAlias('webroot') . Yii::app()->params['uploadFolderTh'] . 't' . $user->photo . '.jpg');
			$this->setPhoto('');
		}
		$this->redirect(array('update', 'id' => Yii::app()->user->id));
	}

	public function setPhoto($name)
	{
		$command = Yii::app()->db->createCommand();
		if ($command->update('users', array(
				'photo' => $name,
			),
			'id=:id', array(':id' => Yii::app()->user->id))
		) {
			return true;
		}
		return false;
	}

	public function actionMail()
	{
		$this->setPageTitle('Розсилка');
		$form = new MailForm;
		if (isset($_POST['MailForm'])) {
			$form->attributes = $_POST['MailForm'];
			$u = User::model()->findAll();
			$mailer = Yii::app()->mail;
			foreach ($u as $a) {
				//send mail for everyone
				$mail = $mailer->newMessage(array(
					'subject' => $form->subject,
					'from'    => Yii::app()->params['mail'],
					'to'      => $a->email,
				));
				$mail->view = array('subscription', 'message' => $form->body);
				if (!$mailer->send($mail)) {
					Yii::app()->user->setFlash('fail', 'Помилка розсилки. Спробуйте пізніше.');
				}
			}
			if (!Yii::app()->user->getFlash('fail')) {
				Yii::app()->user->setFlash('success', 'Розсилка успішно відправлена.');
			}
			$this->refresh();
		}
		$this->render('mail', array(
			'model' => $form
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = User::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
