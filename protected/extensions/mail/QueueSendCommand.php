<?php
class QueueSendCommand extends CConsoleCommand
{
	public $limit = 5;
	public $modelClass = 'MessageDumpModel';

	public function actionIndex($limit = null)
	{
		/** @var $mailer Mailer */
		$mailer = Yii::app()->mail;

		if($limit !== null) {
			$this->limit = (int)$limit;
		}

		foreach($this->loadMessages() as $mail) {
			/** @var MessageDumpModel $mail */
			/** @var Message $message */
			$message = $mail->messageSource;
			echo $mailer->send($message) . "\n";
			$mail->delete();
		}

		return 0;
	}

	protected function loadMessages()
	{
		$className = $this->modelClass;
		/** @var $model MessageDumpModel */
		$model = new $className;
		return $model->findAll(array('limit' => $this->limit));
	}
}