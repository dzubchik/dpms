<?php
/**
 * LogToFilesBehavior class file.
 */
/**
 * @property string $path
 *
 * @deprecated Use Yii::log()
 */
class LogToFilesBehavior extends CBehavior
{
	private  $_path;

	/**
	 * Returns the directory that stores message files.
	 * @return string the directory that stores message files. Defaults to 'protected/runtime/mails'.
	 */
	public function getPath()
	{
		if($this->_path === null) {
			$this->setPath(Yii::app()->runtimePath . '/mails');
		}
		return $this->_path;
	}

	/**
	 * Sets the directory that stores message files.
	 * @param string $path the directory that stores message files.
	 * @throws CException if the directory does not exist or is not writable
	 */
	public function setPath($path)
	{
		if(($storagePath = realpath($path)) === false || !is_dir($storagePath) || !is_writable($storagePath)) {
			throw new CException(Yii::t('yii','Mailer runtime path "{path}" is not valid. Please make sure it is a directory writable by the Web server process.',
				array('{path}'=>$path)));
		}

		$this->_path = $storagePath;
	}
	/**
	 * Declares events and the corresponding event handler methods.
	 * @return array events (array keys) and the corresponding event handler methods (array values).
	 */
	public function events()
	{
		return array('onAfterSend'=>'afterSend',);
	}

	/**
	 * @param MailerEvent $event
	 */
	public function afterSend(MailerEvent $event)
	{
		$file = $this->path . '/' . md5($event->message->id);
		@file_put_contents($file, $event->message->toString());
	}
}
