<?php
/**
 * Mailer class file.
 */

$swiftDir = dirname(__FILE__) . '/vendors/swift/lib';
require_once($swiftDir . '/classes/Swift.php');
Yii::registerAutoloader(array('Swift', 'autoload'));
require_once($swiftDir . '/swift_init.php');

/**
 * @property Swift_Transport $transport
 * @property string $viewPath
 */
class Mailer extends \CApplicationComponent
{
	/** @var bool */
	public $separateRecipients = false;

	/** @var array */
	protected $failedRecipients = array();

	/** @var string */
	private $_viewPath;
	/** @var Swift_Transport The Transport used to send messages */
	private $_transport;

	/**
	 * @return Swift_Transport
	 * @throws CException
	 */
	public function getTransport()
	{
		if($this->_transport === null) {
			$this->setTransport(array(
				'class' => 'Swift_MailTransport',
			));
		}
		return $this->_transport;
	}

	/**
	 * @param array|string $config
	 */
	public function setTransport($config)
	{
		if(is_string($config)) {
			$className = $config;
			$config = array();
		} else if(isset($config['class'])) {
			$className = $config['class'];
			unset($config['class']);
		} else {
			throw new CException(Yii::t('yii','Object configuration must be an array containing a "class" element.'));
		}

		$this->_transport = new $className;
		foreach($config as $option => $value) {
			$method = 'set' . ucfirst($option);
			$this->_transport->$method($value);
		}
	}

	/**
	 * @param Message $message
	 * @param null $failedRecipients
	 * @return int
	 */
	public function send(Message $message)
	{
		Mailer::log(Yii::t('yiiext', 'Sending message {id}.', array(
					'{id}' => $message->id,
				)), CLogger::LEVEL_TRACE);

		$sent = 0;
		if($this->separateRecipients && count($message->to) > 1) {
			$to = $message->to;
			foreach($to as $email => $name) {
				$message->to = array($email => $name);
				$message->generateId();
				if($this->beforeSend($message)) {
					$sent += $this->sendInternal($message);
					$this->afterSend($message);
				}
			}
		} else {
			if($this->beforeSend($message)) {
				$sent += $this->sendInternal($message);
				$this->afterSend($message);
			}
		}

		return $sent;
	}

	/**
	 * @param Message $message
	 * @return int
	 */
	protected function sendInternal(Message $message)
	{
		if(!$this->transport->isStarted()) {
			$this->transport->start();
		}

		try {
			return $this->transport->send($message->message, $this->failedRecipients);
		} catch(Swift_RfcComplianceException $e) {
			/*foreach($message->to as $address => $name) {
				$this->failedRecipients[] = $address;
			})*/
			return -1;
		}
	}

	/**
	 * @param $event
	 */
	public function onBeforeSend($event)
	{
		$this->raiseEvent('onBeforeSend', $event);
	}

	/**
	 * @param $event
	 */
	public function onAfterSend($event)
	{
		$this->raiseEvent('onAfterSend', $event);
	}

	/**
	 * @param $message
	 * @return bool
	 */
	protected function beforeSend($message)
	{
		$event=new MailerEvent($this, $message);
		$this->onBeforeSend($event);
		return $event->isValid;
	}

	/**
	 * @param $message
	 */
	protected function afterSend($message)
	{
		$this->onAfterSend(new MailerEvent($this, $message));
	}

	/**
	 * @param $view
	 * @param $viewData
	 * @return string
	 */
	public function render($view, $viewData)
	{
		$basePath = Yii::app()->viewPath;
		$renderer = Yii::app()->controller;
		$viewFile = $renderer->resolveViewFile($view, $this->viewPath, $basePath, $basePath);

		return $renderer->renderFile($viewFile, $viewData, true);
	}

	/**
	 * @return string
	 */
	public function getViewPath()
	{
		if($this->_viewPath === null) {
			$this->setViewPath(Yii::app()->viewPath . DIRECTORY_SEPARATOR . 'mails');
		}

		return $this->_viewPath;
	}

	/**
	 * @param $path
	 * @throws CException
	 */
	public function setViewPath($path)
	{
		if(($viewPath = realpath($path)) === false || !is_dir($viewPath)) {
			throw new CException(Yii::t('yii','The view path "{path}" is not a valid directory.',
				array('{path}' => $path)));
		}

		$this->_viewPath = $viewPath;
	}

	/**
	 * Create new instance of message.
	 *
	 * @param array $config
	 * @return Message
	 */
	public function newMessage(array $data = array())
	{
		$className = isset($data['class']) ? $data['class'] : 'Message';
		$message = new $className($this);
		foreach($data as $option => $value) {
			$message->$option = $value;
		}
		return $message;
	}

	/**
	 * Mailer logger.
	 * @static
	 * @param $msg
	 * @param string $level
	 */
	public static function log($msg, $level = CLogger::LEVEL_INFO)
	{
		Yii::log($msg, $level, 'application.' . __CLASS__);
	}
}

class MailerEvent extends \CEvent
{
	/** @var Message */
	public $message;
	/** @var bool */
	public $isValid = true;

	public function __construct($sender = null, $message = null, $params = null)
	{
		parent::__construct($sender = null, $params = null);
		$this->message = $message;
	}
}