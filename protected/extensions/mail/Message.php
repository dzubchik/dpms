<?php
/**
 * Message class file.
 *
 * Getters:
 * @property string $id
 * @property string $subject
 * @property int $date
 * @property string $returnPath
 * @property string $sender
 * @property string $from
 * @property string $replyTo
 * @property array $to
 * @property string $cc
 * @property string $bcc
 * @property int $priority
 * @property string $readReceiptTo
 * @property string $charset
 * @property string $format
 *
 * @property string $body
 * @property string|array $view
 * @property Mailer $owner
 * @property Swift_Message $message
 * @property Swift_Mime_HeaderSet $headers
 *
 * @method generateId()
 * @method toString()
 */
class Message extends \CComponent
{
	/** @var Swift_Message */
	private $_message;
	/** @var Swift_Message */
	private $_owner;

	/**
	 * @param Mailer $owner
	 */
	public function __construct(Mailer $owner)
	{
		$this->_owner = $owner;
	}

	/**
	 * Getter to message.
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		try {
			return parent::__get($name);
		} catch(CException $e) {
			$method = 'get' . ucfirst($name);
			if(method_exists($this->message, $method)) {
				return $this->message->$method();
			} else if(property_exists($this->message, $name)) {
				return $this->message->$name;
			} else {
				throw $e;
			}
		}
	}

	/**
	 * Setter to message.
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return mixed|void
	 */
	public function __set($name, $value)
	{
		try {
			parent::__set($name, $value);
		} catch(CException $e) {
			$method = 'set' . ucfirst($name);
			if(method_exists($this->message, $method)) {
				$this->message->$method($value);
			} else if(property_exists($this->message, $name)) {
				$this->message->$name = $value;
			} else {
				throw $e;
			}
		}
	}

	/**
	 * Call Swift_Message methods.
	 *
	 * @param string $method
	 * @param array $parameters
	 * @return mixed
	 */
	public function __call($method, $parameters)
	{
		try {
			return parent::__call($method, $parameters);
		} catch(CException $e) {
			if(method_exists($this->message, $method)) {
				return call_user_func_array(array($this->message, $method), $parameters);
			} else {
				throw $e;
			}
		}
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->message->toString();
	}

	/**
	 * @return Swift_Message
	 */
	public function getMessage()
	{
		if($this->_message === null) {
			$this->_message = Swift_Message::newInstance();
		}

		return $this->_message;
	}

	public function getOwner()
	{
		return $this->_owner;
	}

	/**
	 * Set message body.
	 * @param string $body The message content.
	 * @param string $contentType The content type. Defaults to 'text/html'
	 * @param string|null $charset The message charset. If null, the application charset uses.
	 */
	public function setBody($body = '', $contentType = 'text/html', $charset = null)
	{
		if($charset === null) {
			$charset = Yii::app()->charset;
		}
		$this->message->setBody($body, $contentType, $charset);
	}

	/**
	 * @param string|array $view
	 * @param array|null $viewData
	 */
	public function setView($view, $viewData = null)
	{
		if(is_array($view)) {
			$viewData = $view;
			$view = array_shift($viewData);
		}

		$this->setBody($this->owner->render($view, $viewData));
	}
}
