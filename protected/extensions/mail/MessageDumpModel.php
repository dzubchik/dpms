<?php
/**
 * MessageDumpModel class file.
 */
/**
 * Attributes:
 * @property string $id
 * @property string $message
 * @property Message $messageSource
 *
 * Getters:
 * @property CDbConnection $dbConnection
 */
class MessageDumpModel extends Model
{
	public $id;
	public $message;

	public function tableName()
	{
		return 'mails';
	}

	public function getDbConnection()
	{
		return Yii::app()->db;
	}

	public function getCriteria($condition = '', $params = array())
	{
		if(is_array($condition)) {
			$condition = new CDbCriteria($condition);
		} else if(!$condition instanceof CDbCriteria) {
			$condition = new CDbCriteria(array(
				'condition' => $condition,
				'params' => $params,
			));
		}

		return $condition;
	}

	public function findAll($condition = '', $params = array())
	{
		$criteria = $this->getCriteria($condition, $params);
		$command = $this->dbConnection->commandBuilder->createFindCommand($this->tableName(), $criteria);
		$command->setFetchMode(PDO::FETCH_CLASS, __CLASS__); // | PDO::FETCH_PROPS_LATE
		return $command->queryAll();
	}

	public function find($condition = '', $params = array())
	{
		$criteria = $this->getCriteria($condition, $params);
		$criteria->limit=1;
		return $this->findAll($criteria);
	}

	public function deleteAll($condition = '', $params = array())
	{
		$criteria = $this->getCriteria($condition, $params);
		$command = $this->dbConnection->commandBuilder->createDeleteCommand($this->tableName(), $criteria);
		return $command->execute();
	}

	public function deleteByPk($pk, $condition = '', $params = array())
	{
		$criteria = $this->getCriteria($condition, $params);
		$criteria->compare('id', $pk);
		$criteria->limit = 1;
		return $this->deleteAll($criteria);
	}

	/**
	 * @param string $condition
	 * @param array $params
	 * @return int
	 * @throws CException
	 */
	public function delete($condition = '', $params = array())
	{
		if($this->id === null) {
			throw new CException('Cannot delete new model.');
		}
		return $this->deleteByPk($this->id, $condition, $params);
	}

	/**
	 * @return int
	 */
	public function save()
	{
		$data = array(
			'id' => $this->id,
			'message' => $this->message,
		);
		$command = $this->dbConnection->commandBuilder->createInsertCommand($this->tableName(), $data);
		return $command->execute();
	}

	/**
	 * @param Message $message
	 */
	public function setMessageSource(Message $message)
	{
		$this->message = serialize($message);
	}

	/**
	 * @return Message
	 */
	public function getMessageSource()
	{
		return unserialize($this->message);
	}
}