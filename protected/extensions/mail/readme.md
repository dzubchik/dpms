Mail
=====

The classes helps you to work with email.

**Mailer** - The application component used to send messages.

**DummyMailer** - The application component used to send messages.

**Message** - The component represent a message object.

**MessageDumpModel** - The model used to store mail in DB.

**QueueSendCommand** - The console command to send queue mail from DB.

**CopyToDbBehavior** - The behavior for mailer to save copy of message to DB.

**CopyToFilesBehavior** - The behavior for mailer to save copy of message to file.

**LogToFilesBehavior** - Save the mail content to files. This component is depricated, will be deleting in the future.


Usage
------------

Configure application:

```
'import => array(
	'ext.mail.*',
),
'components' => array(
	'mail' => array(
		'class' => 'Mailer',
		// to use simple php function
		'transport' => array(
			'class' => 'Swift_MailTransport',
		),
		// or to use SMTP protocol
		'transport' => array(
			'class' => 'Swift_SmtpTransport',
			'host' => 'localhost',
			'username' => 'username',
			'password' => '********',
		),
	),
),
```

Create message and send it:
```
public function sendMessageToUsers()
{
	/** @var Mailer $mailer */
	$mailer = Yii::app()->mail;
	/** @var Message $mail */
	$mail = $mailer->newMessage(array(
			'subject' => 'The message subject',
			'from' => Yii::app()->params['robotEmail'],
		));

	foreach(User::model()->findAll() as $user) {
		$mail->to = array($user->email => $user->fullname)
		// use a view file
		$mail->view = array('welcomeMessage', 'text' => 'Hello user.');
		// or set body
		$mail->body = 'Hello user.';
		$mailer->send($mail);
	}
}
```

Example how you can delay sending mail.
------------

Web application config:

```
'components' => array(
	'mail' => array(
		// Use dummy mailer
		'class' => 'DummyMailer',
		// Copy mails ti DB
		'behaviors' => array(
			'CopyToDbBehavior',
		),
	),
),
```

Console application config:

```
'commandMap' => array(
	'mail' => array(
		'class' => 'QueueSendCommand',
		'limit' => 5,
	),
),
'components' => array(
	'mail' => array(
		'class' => 'Mailer',
		'transport' => array(
			'class' => 'Swift_MailTransport',
		),
	),
),
```

Start console command:
```
$ php index-console.php mail
```



Requirements
------------

- Server should be able to run [Yii framework](http://www.yiiframework.com).
- [SWIFT Mailer](http://swiftmailer.org)

License
-------

**Mail** is licensed under New BSD license. That allows proprietary use, and for
the software released under the license to be incorporated into proprietary
products. Works based on the material may be released under a proprietary license
or as closed source software. It is possible for something to be distributed
with the BSD License and some other license to apply as well.
