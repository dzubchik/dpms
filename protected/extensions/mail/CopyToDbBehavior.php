<?php
/**
 * CopyToDbBehavior class file.
 */
class CopyToDbBehavior extends CBehavior
{
	public $modelClass = 'MessageDumpModel';

	/**
	 * Declares events and the corresponding event handler methods.
	 * @return array events (array keys) and the corresponding event handler methods (array values).
	 */
	public function events()
	{
		return array('onAfterSend'=>'afterSend',);
	}

	/**
	 * @param MailerEvent $event
	 */
	public function afterSend(MailerEvent $event)
	{
		$className = $this->modelClass;
		/** @var $model MessageDumpModel */
		$model = new $className;
		$model->messageSource = $event->message;
		$model->save();
	}
}
