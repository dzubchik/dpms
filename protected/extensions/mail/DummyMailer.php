<?php
/**
 * DummyMailer class file.
 */
class DummyMailer extends Mailer
{
	protected function sendInternal(Message $message)
	{
		return count($message->to);
	}
}