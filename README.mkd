INSTALLATION
------------
1. Install php5-cli and php5-mysql, php5-gd, ant
2. Create database ```physics``` with access user ```physics``` and password ```DBPASSWORD```
3. Run `ant` in root directory
4. Run ```./protected/yiic migrate up```