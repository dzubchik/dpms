#!/bin/bash

echo "Please enter database credentials:"
echo DB HOST[localhost]:
read HOST
if [ -z "${HOST}" ]; then
    HOST=localhost
fi
echo DB USER[root]:
read USER
if [ -z "${USER}" ]; then
    USER=root
fi
echo DB PASSWORD[without pass]:
read PASS
if [ -z "${PASS}" ]; then
    PASS=""
fi
echo DB DATABASE[physics]:
read DB
if [ -z "${DB}" ]; then
    DB="physics"
fi

CONFIG="./protected/config/main.php"
cp protected/config/main-simple.php $CONFIG

sed -i "s/HOST/$HOST/g" $CONFIG
sed -i "s/USER/$USER/g" $CONFIG
sed -i "s/PASSWORD/$PASS/g" $CONFIG
sed -i "s/DATABASE/$DB/g" $CONFIG

mysql -h $HOST -u$USER -p$PASSWORD --database $DATABASE < ./protected/data/dump.sql
